FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

# Set the env variables to non-interactive
ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_PRIORITY critical
ENV DEBCONF_NOWARNINGS yes
ENV PANDOC_VERSION 2.17.0.1

RUN apt-get -qq update && \
    apt-get -qq -y install wget texlive-latex-base texlive-fonts-recommended && \
    apt-get -qq -y install texlive-fonts-extra texlive-latex-extra && \
    apt-get -qq -y install lmodern && \
    apt-get clean

RUN wget https://github.com/jgm/pandoc/releases/download/${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}-1-amd64.deb && \
    dpkg -i pandoc* && \
    rm pandoc* && \
    apt-get clean

RUN pip install pandoc-latex-environment

RUN mkdir -p /app/code
RUN mkdir -p /app/data
RUN mkdir -p /app/data/input
RUN mkdir -p /app/data/output
RUN mkdir -p /app/data/done
RUN mkdir -p /app/data/public

WORKDIR /app/code

ADD package.json package-lock.json server.js start.sh process.sh /app/code/
ADD awesomebox.sty /app/data/input/
ADD eisvogel.latex /app/data/input/
ADD dl.txt /app/data/output/

# add webserver
RUN npm install
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

ADD index.html /app/data/public/

CMD [ "/app/code/start.sh" ]
