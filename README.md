This app is a custom (not officially developed or maintained) app packaged for Cloudron. 

It packages an instance of the excellent document conversion utility Pandoc (https://pandoc.org).

Pandoc only runs from the command line.  By packaging it into a web app, a consistent publishing environment can be maintained in a single location (avoiding multiple local installs which might change over time) and be accessible from any device, even without Pandoc and its utilities installed. 

It is based on Cloudron's tutorial-supervisor app and the pandoc docker app from Viktor Petersson <vpetersson@wireload.net> (https://github.com/vpetersson/docker-pandoc).

Upload a source document in Markdown (.md) format plus any images to be included.

This app will process them using Pandoc (https://pandoc.com) with a tweaked <a href="https://github.com/Wandmalfarbe/pandoc-latex-template">eisvogel template</a> and <a href="https://www.ctan.org/tex-archive/graphics/awesomebox">awesomebox.sty</a> to create a beautiful PDF, without the hassle of manual formatting in Word etc.
A file awesomebox.pdf is included to show what is possible for styling effects.

A sample document (sample.md) with sample cover (sample-background.pdf) and a sample logo / image (sample-logo.png) are provided, along with what the app will produce (sample.pdf).

Once the source Markdown and related images files are uploaded, click the button to process them.

The app will create a PDF, which must be downloaded to the local computer, as it cannot be retrieved later.

The source Markdown file will be moved to /app/data/done (in future version, it will be deleted).

Image files will not be moved, in case they are required for subsequent PDF creations, e.g.on updating and re-uploading the source Markdown file.

NB : currently source image files are not removed  in case they are needed for re-running the document production.  Therefore they might be overwritten by other users of the same app uploading a file of the same name but different content.  Your document will then be re-created with the wrong image.  If so, re-upload them and retry.  This issue will be addressed in a future version.

Authentication may be added later (currently not required under initial use case).
