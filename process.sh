#!/bin/bash

# Find all LaTeX and Markdown files
MDFILES=$(find /app/data/input -regex '.*\.\(md\|markdown\|tex\)')

cd /app/data/input

if [ -n "$MDFILES" ]; then
    for i in "$MDFILES"; do
        INFILE=$(basename "$i")
        OUTFILE=$(echo "$INFILE" | sed 's/\.\(md\|markdown\|tex\)/\.pdf/g')

        echo "Converting $INFILE..."
# original
#        pandoc -V geometry:margin=1in -t latex -o "$OUTFILE" "$INFILE"
        pandoc "$INFILE" -o "$OUTFILE" --pdf-engine=pdflatex --from markdown --template ./eisvogel.latex --filter pandoc-latex-environment --listings
        mv "$INFILE" /app/data/done/
        mv "$OUTFILE" /app/data/output/
        echo "$OUTFILE" > /app/data/output/dl.txt
    done
else
    echo "No LaTeX or Markdown files found. Exiting."
fi
