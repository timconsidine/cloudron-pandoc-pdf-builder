---
title: "SAMPLE DOCUMENT"
subtitle: "Demonstration of features"
author: "Tim Considine"
date: "2022-01-29"
lang: "en-GB"
titlepage: true
titlepage-background: "sample-background.pdf"
titlepage-color: "008080"
titlepage-text-color: "333333"
titlepage-rule-color: "FBC81A"
titlepage-rule-height: 4
header-left: "\\thetitle"
header-center: "\\hspace{1cm}"
header-right: "\\leftmark"
footer-left: "\\theauthor"
footer-center: "\\thedate"
footer-right: "Page \\thepage"
logo: "sample-logo.png"
logo-width: 90mm
colorlinks: true
header-includes:
- |
  ```{=latex}
  \usepackage{awesomebox}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  noteblock: [note]
  tipblock: [tip]
  warningblock: [warning]
  cautionblock: [caution]
  importantblock: [important]
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]

...


# CONTEXT

This is just a sample document to test the Cloudron Pandon PDF Builder.

It is just a markdown document.  Upload it to the app with any images and cover background.

::: important
This is an example of using awesomebx.sty to add styling to content.

Have a look at some other features. 
:::

Make sure you understand and tweak the preamble which is at the beginning of the document.

## Preamble

This has core information to setup the document for building a PDF.

\newpage 
# FEATURES

You can include images and other features in the body of the document.

They should be in the same directory as the document.

## Cover logo

One logo you may want to use is on the cover page.

It can be references in the preamble.

## Cover Background

Also in the preamble, you can specify a cover.

This is just a PDF with any graphics you want to show on the cover page.

The document title and other document data will be over-printed onto this background.

::: warning
This is another styling feature to draw attention to content.

You can add multiple lines.
:::

The styling features you can use are :
```
  noteblock: [note]
  tipblock: [tip]
  warningblock: [warning]
  cautionblock: [caution]
  importantblock: [important]
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
```

### Subheadings

This is a 3rd level subheading.
You can use standard markdown for this, e.g. `###`.

> You can also add a blockquote for certain content

Other standard markdown can be used.

- bullets

	-  sub-bullets

- bullets

Check guides on the internet for writing Markdown.

## Images

An image is included in the body of the document using standard Markdown.
  
    ```![Sample Image](sample-logo.png)```

![Sample Image](sample-logo.png)

## Highlight text

You can highlight text using `a back-tick` character at start and end of text.

NB : this is not the same as a single quote.

---

::: box

[LOGO]

**SAMPLE TEXT**

sample text
\vskip 0.3em

Occasionally you may need to include some `latex` instructions to control document layout and appearance.
EG :  `\vskip 0.3em`
 
\vskip 0.3em

[website]

[contact point]
:::


::: box

![](awesomebox-p1.pdf)
:::


\color{red}

\center
end of document

--- 
