"use strict";

var http = require("http");
var url = require('url');  
var fs = require('fs');  
var formidable = require('formidable');
var contentDisposition = require('content-disposition');
var destroy = require('destroy');
var onFinished = require('on-finished');

var filePath = '';
var fileName = '';

var server = http.createServer(function (request, response) {
    var path = url.parse(request.url).pathname;  
    switch (path) {  
        case '/':
//            fs.readFile(__dirname + path, function(error, data) {  
            fs.readFile('/app/data/public/index.html', function(error, data) {  
                if (error) {  
                    response.writeHead(404);  
                    response.write(error);  
                    response.end();  
                } else {  
                    response.writeHead(200, {  
                        'Content-Type': 'text/html'  
                    });  
                    response.write(data);  
                    response.end();  
                }  
            });  
            break;
        case '/fileupload':
            var form = new formidable.IncomingForm();
            form.parse(request, function (err, fields, files) {
              var oldpath = files.filetoupload.filepath;
              var newpath = '/app/data/input/' + files.filetoupload.originalFilename;
              fs.copyFile(oldpath, newpath, function (err) {
                if (err) throw err;
                response.writeHead(200, {'Content-Type': 'text/html'});
                response.write('<h1>Pandoc PDF Generator</h1>');
                response.write('<hr>');
                response.write('File uploaded !<br><br>');
                response.write('<button name="button" onclick="location.href=\'/\'">Upload another (e.g. images) ?</button>');
                response.write('<br><br>');
                response.write('<button name="button" onclick="location.href=\'/processfile\'">Or click here to process file(s) uploaded.</button>');
                response.end();
                fs.rm(oldpath, function (err) {
                    if (err) throw err;
                });
              });
            });
            break; 
        case '/processfile':
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write('<h1>Pandoc PDF Generator</h1>');
            response.write('<hr>');
            response.write('Processing file ...<br><br>');
            const { exec } = require('child_process');
            exec('/app/code/process.sh', (error, stdout, stderr) => {
              if (error) {
                console.error(`exec error: ${error}`);
                return;
              }
              console.log(`stdout: ${stdout}`);
              console.error(`stderr: ${stderr}`);
            });
            response.write('Processed file ...<br><br>');
            response.write('<button name="button" onclick="location.href=\'/download\'">Download PDF</button>');
            response.write('<br><br>');
            response.write('<button name="button" onclick="location.href=\'/\'">Return to Home Page</button>');
            response.end();
            break; 
        case '/download':
            fs.readFile('/app/data/output/dl.txt', function(error, data) {  
                if (error) {  
                    response.writeHead(404);  
                    response.write(error);  
                    response.end();  
                } else {  
                    filePath = '/app/data/output/' + data.toString().trim();
                    fileName =  data.toString().trim();
                    var file = fs.createReadStream(filePath);
                    var stat = fs.statSync(filePath);
                    response.setHeader('Content-Length', stat.size);
                    response.setHeader('Content-Type', 'application/pdf');
                    response.setHeader('Content-Disposition', 'attachment; filename='+fileName);
                    file.pipe(response);
                }  
            }); 
            break;
        default:  
            response.writeHead(404);  
            response.write("server.js says : ooops this doesn't exist - 404");  
            response.end();  
            break;  
    }  
});

server.listen(3000);

console.log("Server running at port 3000");
